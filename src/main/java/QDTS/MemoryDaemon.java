package QDTS;

public class MemoryDaemon implements Runnable {
	private long memoryUsed = 0;
	public MemoryDaemon() {
		
	}

	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;

		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != getMemoryUsed()) {
				System.out.println("\tMemory used = " + used);
				setMemoryUsed(used);
			}
		}
	}

	public long getMemoryUsed() {
		return memoryUsed;
	}

	public void setMemoryUsed(long memoryUsed) {
		this.memoryUsed = memoryUsed;
	}

}
