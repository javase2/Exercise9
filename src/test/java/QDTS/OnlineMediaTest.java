package QDTS;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for Exercise9.
 */

@DisplayName("9th exercise Test")
public class OnlineMediaTest {
    private final static PipedOutputStream pipe = new PipedOutputStream();
    private final static ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final static ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final static InputStream originalIn = System.in;
    private final static PrintStream originalOut = System.out;
    private final static PrintStream originalErr = System.err;

    @BeforeAll
    public static void catchIO() throws IOException {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        System.setIn(new PipedInputStream(pipe));
    }

    @AfterAll
    public static void releaseIO() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalIn);
    }


    @Test
    @DisplayName("9th exercise Test")
    public void OnlineMediaTestMain() throws Exception {

        OnlineMedia.main(new String[0]);
        String[] output = out.toString().split(System.lineSeparator());
        assertNotNull(output[0]);
        //assertEquals("dvd1.title = IBM Dance Party", output[0]);

        assertNotNull(output[1]);
        //assertEquals("Playing DVD: IBM Dance Party", output[1]);

        assertNotNull(output[2]);
        assertNotNull(output[3]);
        //assertEquals("DVD length: 87", output[3]);

        assertNotNull(output[4]);
        //assertEquals("dvd2.title = IBM Kids Sing-along", output[4]);

        assertNotNull(output[5]);
        //assertEquals("Playing DVD: IBM Kids Sing-along", output[5]);

        assertNotNull(output[6]);
        //assertEquals("DVD length: 124", output[6]);

        assertNotNull(output[7]);
        //assertEquals("dvd3.title = IBM Smarter Planet", output[7]);

        assertNotNull(output[8]);
        //assertEquals("Playing DVD: IBM Smarter Planet", output[8]);
        
        assertNotNull(output[9]);
        //assertEquals("DVD length: 90", output[9]);

        assertNotNull(output[10]);
        //assertEquals("Book title = Java Programming", output[10]);

        assertNotNull(output[11]);
        //assertEquals("cd1.title = IBM Symphony", output[11]);

        assertNotNull(output[12]);
        //assertEquals("Cost of CD = 29.95", output[12]);

        assertNotNull(output[14]);
        //assertEquals("The tracks currently are in the order: ", output[14]);

        assertNotNull(output[15]);
        //assertEquals("t1.title = Warmup", output[15]);

        assertNotNull(output[16]);
        //assertEquals("t2.title = Scales", output[16]);

        assertNotNull(output[17]);
        //assertEquals("t3.title = Introduction", output[17]);

        assertNotNull(output[19]);
        //assertEquals("Playing CD: IBM Symphony", output[19]);

        assertNotNull(output[20]);
       // assertEquals("CD length:10", output[20]);

        assertNotNull(output[21]);
        //assertEquals("Playing track: Warmup", output[21]);

        assertNotNull(output[22]);
        //assertEquals("Track length: 3", output[22]);

        assertNotNull(output[23]);
        //assertEquals("Playing track: Scales", output[23]);

        assertNotNull(output[24]);
        //assertEquals("Track length: 4", output[24]);

        assertNotNull(output[25]);
        //assertEquals("Playing track: Introduction", output[25]);

        assertNotNull(output[26]);
        //assertEquals("Track length: 3", output[26]);

        //assertEquals("------------------------------------", output[27]);

        assertNotNull(output[28]);
        //assertEquals("The tracks in sorted order are: ", output[28]);

        assertNotNull(output[29]);
        
        assertNotNull(output[31]);
        //assertEquals("Introduction", output[31]);

        assertNotNull(output[32]);
        //assertEquals("Scales", output[32]);

        assertNotNull(output[33]);
        //assertEquals("Warmup", output[33]);

        //assertEquals("------------------------------------", output[33]);
        //assertEquals("The total length of the CD is 10", output[35]);
        //assertEquals("Total Cost of the Order is: 163.83", output[36]);
    }
}
