# Exercise9
Исключения

#Содержание файла "PlayerException.java"

public class PlayerException extends Exception {

	public PlayerException() {
		
	}

	public PlayerException(String message) {
		super(message);
	}

	public PlayerException(Throwable cause) {
		super(cause);
	}

	public PlayerException(String message, Throwable cause) {
		super(message, cause);
	}

	public PlayerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}


#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise9",
                "component": "maven"
            },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise9",
                "component": "maven"
             },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia test",
            "command": "mvn verify",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise9",
                "component": "maven"
             },
            "problemMatcher": []
        }
    ]
}
